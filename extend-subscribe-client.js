// Write your package code here!

// Variables exported by this module can be imported by other packages and
// applications. See extend-subscribe-tests.js for an example of importing.
export const name = 'extend-subscribe';




const extendedSubscribeCalls = {

};

var origMeteor_subscribe = Meteor.subscribe;
Meteor.subscribe = function(name){

  var extendFields = arguments.length >= 3 && _.isObject(arguments[2]) && arguments[2].extendFields;
  var selector = arguments.length >= 2 && _.isObject(arguments[1]) && arguments[1];
  var sub = origMeteor_subscribe.apply(this, _.toArray(arguments));

  if(extendFields){
    extendedSubscribeCalls[sub.subscriptionId] = {
      sub,
      selector,
      extendFields: extendFields,
    };
  }
  return sub;
};

function mergeFields(fields, selector, extendFields){
  _.each(extendFields, function(value, key){
    var obj;
    if(value !== false){
      keyParts = key.split(".");
      if(!fields){
        fields = {};
      }
      obj = fields;
      while(keyParts.length > 1){
        var keyPart = keyParts.splice(0, 1)[0];
        if(!obj[keyPart]){
          obj[keyPart] = {};
        }
        obj = obj[keyPart];
      }
    }
    if(value === true && !obj[keyParts[0]] && _.isObject(selector)){
      obj[keyParts[0]] = selector[key];
    }
    else if(value !== false && !obj[keyParts[0]]){
      obj[keyParts[0]] = value;
    }
  });
}
var orig_process_added = Meteor.default_connection._process_added;
Meteor.default_connection._process_added = function(msg, updates){
  if(msg.sId && extendedSubscribeCalls[msg.sId]){
    if(!msg.fields){
      msg.fields = {};
    }
    mergeFields(msg.fields, extendedSubscribeCalls[msg.sId].selector, extendedSubscribeCalls[msg.sId].extendFields);
  }
  orig_process_added.apply(this, _.toArray(arguments));
};

var orig_process_changed = Meteor.default_connection._process_changed;
Meteor.default_connection._process_changed = function(msg, updates){
  if(msg.sId && extendedSubscribeCalls[msg.sId]){
    if(!msg.fields){
      msg.fields = {};
    }
    mergeFields(msg.fields, extendedSubscribeCalls[msg.sId].selector, extendedSubscribeCalls[msg.sId].extendFields);
  }
  orig_process_changed.apply(this, _.toArray(arguments));
};
