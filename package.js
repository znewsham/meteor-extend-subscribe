Package.describe({
  name: 'znewsham:extend-subscribe',
  version: '0.0.3',
  summary: 'extend subscriptions with constant values on the client',
  description: "Provides a network efficient way to populate documents with constant fields on a per subscription basis, e.g., populating the documents with the fields used to search for them.",
  // URL to the Git repository containing the source code for this package.
  git: 'https://bitbucket.org/znewsham/meteor-extend-subscribe',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.5');
  api.use('ecmascript');
  api.use('underscore');
  api.use("ddp");
  api.use('mongo@1.1.14');
  api.use('meteorhacks:meteorx@1.4.1', ['server']);
  api.mainModule('extend-subscribe-client.js',["client"]);
  api.mainModule('extend-subscribe-server.js',["server"]);
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('znewsham:extend-subscribe');
  api.use("ddp");
  api.use('mongo@1.1.14');
  api.use('underscore');
  api.mainModule('extend-subscribe-tests.js');
});
