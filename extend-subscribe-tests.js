// Import Tinytest from the tinytest Meteor package.
import { Tinytest } from "meteor/tinytest";

// Import and rename a variable exported by extend-subscribe.js.
import { name as packageName } from "meteor/znewsham:extend-subscribe";

var testCollection = new Mongo.Collection("extend-subscribe-docs");
if(Meteor.isServer){
  testCollection.allow({
    update(){
      return true;
    }
  });
  Meteor.methods({
    resetDB(){
      testCollection.remove({});
      testCollection.insert({
        _id: "doc1",
        userId: "user1",
        name: "test1",
        object: {
          key: "value"
        }
      });
      testCollection.insert({
        _id: "doc2",
        userId: "user1",
        name: "test2"
      });
      testCollection.insert({
        _id: "doc3",
        userId: "user2",
        name: "test3"
      });
    }
  });

  Meteor.publish("extend-subscribe-docs", function(query, options){
    return testCollection.find(query, options);
  });
  Meteor.publish("docs", function(query, options){
    return testCollection.find(query, options);
  });
}

if(Meteor.isClient){
  Tinytest.addAsync("extend-subscribe - resetDB", function(test, next){
    Meteor.call("resetDB", function(){
      next();
    });
  });
  Tinytest.addAsync("extend-subscribe - regular subscription count", function(test, next){
    var sub = Meteor.subscribe("extend-subscribe-docs", {userId: "user1"}, {},
    function(){
      test.equal(testCollection.find({userId: "user1"}).count(), 2);
      sub.stop();
      next();
    });
  });

  Tinytest.addAsync("extend-subscribe - subscription to regular pub doesn't send back subscriptionId messages", function(test, next){
    var orig_process_added = Meteor.default_connection._process_added;
    Meteor.default_connection._process_added = function(msg){
      test.equal(msg.subscriptionId, undefined);
      orig_process_added.apply(this, _.toArray(arguments));
    };
    var sub = Meteor.subscribe("docs", {userId: "user1"}, {},
    function(){
      test.equal(testCollection.find({userId: "user1"}).count(), 2);
      sub.stop();
      Meteor.default_connection._process_added = orig_process_added;
      next();
    });
  });

  Tinytest.addAsync("extend-subscribe - regular subscription doesn't contain search fields when not requested", function(test, next){
    var sub = Meteor.subscribe("extend-subscribe-docs", {userId: "user1"}, {fields: {_id: true}},
    function(){
      test.equal(testCollection.find({}).fetch().length, 2);
      test.equal(testCollection.find({userId: "user1"}).fetch().length, 0);
      sub.stop();
      next();
    });
  });

  Tinytest.addAsync("extend-subscribe - regular subscription contains search fields when requested", function(test, next){
    var sub = Meteor.subscribe("extend-subscribe-docs", {userId: "user1"}, {fields: {userId: true}},
    function(){
      test.equal(_.where(testCollection.find({userId: "user1"}).fetch(), {userId: "user1"}).length,2);
      sub.stop();
      next();
    });
  });

  Tinytest.addAsync("extend-subscribe - extended subscription(added) contains search fields when not requested, but included in options.extendedFields", function(test, next){
    var sub = Meteor.subscribe("extend-subscribe-docs", {userId: "user1"}, {fields: {_id: true}, extendFields: {userId: true}},
    function(){
      test.equal(_.where(testCollection.find({userId: "user1"}).fetch(), {userId: "user1"}).length, 2);
      sub.stop();
      next();
    });
  });

  Tinytest.addAsync("extend-subscribe - extended subscription(query changed to remove element) contains search fields when not requested, but included in options.extendedFields", function(test, next){
    var sub = Meteor.subscribe("extend-subscribe-docs", {userId: "user1"}, {fields: {_id: true}, extendFields: {userId: true}},
    function(){
      test.equal(_.where(testCollection.find({userId: "user1"}).fetch(), {userId: "user1"}).length, 2);
      testCollection.update({_id: "doc1"}, {$set: {userId: "userX"}}, function(){
        test.equal(_.where(testCollection.find({userId: "user1"}).fetch(), {userId: "user1"}).length, 1);
        sub.stop();
        next();
      });
    });
  });

  Tinytest.addAsync("extend-subscribe - extended subscription(query changed to add element) contains search fields when not requested, but included in options.extendedFields", function(test, next){
    var sub = Meteor.subscribe("extend-subscribe-docs", {userId: "user1"}, {fields: {_id: true}, extendFields: {userId: true}},
    function(){
      test.equal(_.where(testCollection.find({userId: "user1"}).fetch(), {userId: "user1"}).length, 1);
      testCollection.update({_id: "doc1"}, {$set: {userId: "user1"}}, function(){
        test.equal(_.where(testCollection.find({userId: "user1"}).fetch(), {userId: "user1"}).length, 2);
        sub.stop();
        next();
      });
    });
  });

  Tinytest.addAsync("extend-subscribe - extended subscription with dot.notation selector contains search fields when not requested, but included in options.extendedFields", function(test, next){
    var sub = Meteor.subscribe("extend-subscribe-docs", {"object.key": "value"}, {fields: {_id: true}, extendFields: {"object.key": true}},
    function(){
      test.equal(_.filter(testCollection.find({"object.key": "value"}).fetch(), doc=>doc.object.key == "value").length, 1);
      sub.stop();
      next();
    });
  });

  Tinytest.addAsync("extend-subscribe - extended subscription with constant value works", function(test, next){
    var sub = Meteor.subscribe("extend-subscribe-docs", {userId: "user1"}, {fields: {_id: true}, extendFields: {constant: "test"}},
    function(){
      test.equal(testCollection.find({constant: "test"}).fetch().length, 2);
      sub.stop();
      next();
    });
  });


  Tinytest.addAsync("extend-subscribe - existing documents will be updated as required", function(test, next){
    var sub1 = Meteor.subscribe("extend-subscribe-docs", {userId: "user1"}, {fields: {_id: true}},
    function(){
      test.equal(testCollection.find({constant: "test"}).fetch().length, 0);
      test.equal(testCollection.find().fetch().length, 2);
      var sub = Meteor.subscribe("extend-subscribe-docs", {userId: "user1"}, {fields: {_id: true}, extendFields: {constant: "test"}},
      function(){
        test.equal(testCollection.find({constant: "test"}).fetch().length, 2);
        sub.stop();
        sub1.stop();
        next();
      });
    });
  });

  Tinytest.addAsync("extend-subscribe - extended subscription with constant value does not replace returned value", function(test, next){
    var sub = Meteor.subscribe("extend-subscribe-docs", {userId: "user1"}, {fields: {userId: true}, extendFields: {userId: "test"}},
    function(){
      test.equal(testCollection.find({userId: "test"}).fetch().length, 0);
      test.equal(testCollection.find({userId: "user1"}).fetch().length, 2);
      sub.stop();
      next();
    });
  });

  Tinytest.addAsync("extend-subscribe - extendFields values that change will be updated", function(test, next){
    var sub1 = Meteor.subscribe("extend-subscribe-docs", {userId: "user1"}, {fields: {userId: true}},
    function(){
      test.equal(testCollection.find({status: "active"}).fetch().length, 0);
      test.equal(testCollection.find().fetch().length, 2);
      var sub = Meteor.subscribe("extend-subscribe-docs", {userId: "user1"}, {fields: {userId: true}, extendFields: {status: "active"}},
      function(){
        test.equal(testCollection.find({status: "active"}).fetch().length, 2);
        testCollection.update({_id: "doc1"}, {$set: {status: "inactive"}}, function(){
          test.equal(testCollection.find({status: "active"}).fetch().length, 1);
          sub.stop();
          sub1.stop();
          next();
        });
      });
    });
  });

}
