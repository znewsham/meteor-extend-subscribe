
var originalSession_sendAdded = MeteorX.Session.prototype.sendAdded;
MeteorX.Session.prototype.sendAdded = function(coll, id, fields, subscriptionId) {
  if(subscriptionId && this._namedSubs[subscriptionId].sendSubscriptionId){

    //delete all extendFields found in fields (so we don't return them)
    _.keys(this._namedSubs[subscriptionId].extendFields || {}).forEach(function(key){
      delete fields[key];
    });

    var self = this;
    if (self._isSending) {
      self.send({
        msg: "added",
        collection: coll,
        id: id,
        fields: fields,
        sId: subscriptionId
      });
    }
  }
  else {
    return originalSession_sendAdded.call(this, coll, id, fields);
  }
};

var originalSession_sendChanged = MeteorX.Session.prototype.sendChanged;
MeteorX.Session.prototype.sendChanged = function(coll, id, fields, subscriptionId) {
  var self = this;
  if(subscriptionId && this._namedSubs[subscriptionId].sendSubscriptionId){
    //if we aren't sending any changes, and aren't forcing the change, return.
    //The forceChanged is necessary to allow the subscription on collections with/without extendFields.
    if (_.isEmpty(fields) && !this._namedSubs[subscriptionId].forceChanged){
      return;
    }
    //delete all extendFields found in fields (so we don't return them)
    _.keys(this._namedSubs[subscriptionId].extendFields || {}).forEach(function(key){
      delete fields[key];
    });
    if (self._isSending){
      self.send({
        msg: "changed",
        collection: coll,
        id: id,
        fields: fields,
        sId: subscriptionId
      });
    }
  }
  else {
    return originalSession_sendChanged.call(this, coll, id, fields);
  }
};

var origSession_added = MeteorX.Session.prototype.added;
MeteorX.Session.prototype.added = function(subscriptionHandle, collectionName, id, fields){

  var args = _.toArray(arguments);
  var extendFields = (this._namedSubs[subscriptionHandle.slice(1)] || {}).extendFields || {};

  if(extendFields){
    this._namedSubs[subscriptionHandle.slice(1)].forceChanged = _.keys(extendFields).some(function(key){
      return !_.has(fields, key);
    });
  }
  var res = origSession_added.apply(this,args);
  this._namedSubs[subscriptionHandle.slice(1)].forceChanged = false;
  return res;
};

var originalSessionCollectionView_added = MeteorX.SessionCollectionView.prototype.added;
MeteorX.SessionCollectionView.prototype.added =  function (subscriptionHandle, id, fields) {
  var self = this;
  var maybeForce = false;
  var docView = self.documents[id];
  var added = false;
  if (!docView) {
    added = true;
    docView = new MeteorX.SessionDocumentView();
    self.documents[id] = docView;
  }
  docView.existsIn[subscriptionHandle] = true;
  var changeCollector = {};
  _.each(fields, function (value, key) {
    docView.changeField(subscriptionHandle, key, value, changeCollector, true);
  });
  if (added){
    self.callbacks.added(self.collectionName, id, changeCollector, subscriptionHandle.slice(1));
  }
  else{
    self.callbacks.changed(self.collectionName, id, changeCollector, subscriptionHandle.slice(1));
  }
};

var originalSubscription_added = MeteorX.Subscription.prototype.added;
MeteorX.Subscription.prototype.added = function(){
  return originalSubscription_added.apply(this, _.toArray(arguments));
};

var origMeteor_publish = Meteor.publish;
Meteor.publish = function(name, func){
  var self = this;
  return origMeteor_publish.call(this, name, function(){
    //if we're passing in an options object, which contains the extendFields property and the current pubInvocation is named:
      //we need to make a note that we should send the subscriptionId back, as long as there are some extendFields to return
    if(arguments.length >= 2 && _.isObject(arguments[1]) && arguments[1].extendFields){
      var pubInvocation = DDP._CurrentPublicationInvocation.get();
      if(pubInvocation._session._namedSubs[pubInvocation._subscriptionId]){
        //we also need to store the extendFields, so we can fake reactivity on change in certain situations.
        pubInvocation._session._namedSubs[pubInvocation._subscriptionId].extendFields = arguments[1].extendFields;
        var _arguments = arguments;
        //for each requested extendFields, we need to check whether the field is requested
          //if it is we remove it from the list of requested extendFields (this is necessary so we don't send back the data, but do observe for changes).
          //if it isn't, we add it to the list of requested fields.
        if(arguments[1].fields && !arguments[1].nonReactiveExtend){
          _.keys(arguments[1].extendFields).forEach(function(key){
            if(_arguments[1].fields[key] === true){
              delete _arguments[1].extendFields[key];
            }
            else {
              _arguments[1].fields[key] = true;
            }
          });
        }
        pubInvocation._session._namedSubs[pubInvocation._subscriptionId].sendSubscriptionId = _.keys(arguments[1].extendFields).length !== 0;
      }
    }
    return func.apply(this, _.toArray(arguments));
  });
};
